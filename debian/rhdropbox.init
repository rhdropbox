#! /bin/sh
### BEGIN INIT INFO
# Provides:          rhdropbox
# Required-Start:    $syslog
# Required-Stop:     
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start radio helsinki dropbox daemon at boot time
# Description:       Enables dropboxes
### END INIT INFO
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/bin/rhdropbox
CONFIG_DIR=/etc/rhdropbox
NAME=rhdropbox
DESC=rhdropbox
VARRUN_DIR=/var/run/$NAME

test -x $DAEMON || exit 0

# Include rhdropbox defaults if available
if [ -f /etc/default/rhdropbox ] ; then
  . /etc/default/rhdropbox
fi

start_db () {
  STATUS="OK"
  if [ -f $CONFIG_DIR/$DBNAME/config ] ; then
    NEWFILE=''
    test -f  $CONFIG_DIR/$DBNAME/newfile.sh && NEWFILE="-x $CONFIG_DIR/$DBNAME/newfile.sh"
    CHROOTDIR=`grep '^chroot' < $CONFIG_DIR/$DBNAME/config | sed 's/chroot\s*//'`
    if [ -n "$CHROOTDIR" ] ; then
      test -d $CHROOTDIR || mkdir -p $CHROOTDIR
    fi
    test -d $VARRUN_DIR || mkdir -p $VARRUN_DIR
    chmod 777 $VARRUN_DIR
    DAEMONARG=`sed 's/#.*//' < $CONFIG_DIR/$DBNAME/config | grep -e '\w' | sed  's/^/--/' | tr '\n' ' '`
    $DAEMON --write-pid $VARRUN_DIR/$DBNAME.pid $NEWFILE $DAEMONOPTS $DAEMONARG || STATUS="FAILED"
  else
    STATUS="no config found"
  fi
  echo -n "($STATUS)"
}

stop_db () {
  kill `cat $PIDFILE` || true
  rm $PIDFILE
}

set -e
case "$1" in
  start)
  echo -n "Starting $DESC:"
  if test -z "$2" ; then
    if [ -f $CONFIG_DIR/autostart ] ; then
      for DBNAME in `sed 's/#.*//'  <  $CONFIG_DIR/autostart | grep -e '\w'`; do
        echo -n " $DBNAME"
        start_db
      done
    else
      echo " no config found"
      exit 1;
    fi
  else
    while shift ; do
      [ -z "$1" ] && break
      DBNAME=$1
      echo -n " $DBNAME"
      start_db
    done
  fi
  echo "."
  ;;
  stop)
  echo -n "Stoping $DESC:"
  if test -z "$2" ; then
    for PIDFILE in `ls $VARRUN_DIR/*.pid 2> /dev/null`; do
      DBNAME=`echo $PIDFILE | cut -c20-`
      DBNAME=${DBNAME%%.pid}
      echo -n " $DBNAME"
      stop_db
    done
  else
    while shift ; do
      [ -z "$1" ] && break
      if test -e $VARRUN_DIR/$1.pid ; then
        PIDFILE=`ls $VARRUN_DIR/$1.pid 2> /dev/null`
        DBNAME=`echo $PIDFILE | cut -c20-`
        DBNAME=${DBNAME%%.pid}
        echo -n " $DBNAME"
        stop_db
      else
        echo -n " (failure: No such tunnel is running: $1)"
      fi
    done
  fi
  echo "."
  ;;
  reload)
  echo -n "Reloading $DESC:"
  if test -z "$2" ; then
    for PIDFILE in `ls $VARRUN_DIR/*.pid 2> /dev/null`; do
      DBNAME=`echo $PIDFILE | cut -c20-`
      DBNAME=${DBNAME%%.pid}
      echo -n " $DBNAME"
      stop_db
      start_db
    done
  else
    while shift ; do
      [ -z "$1" ] && break
      if test -e $VARRUN_DIR/$1.pid ; then
        PIDFILE=`ls $VARRUN_DIR/$1.pid 2> /dev/null`
        DBNAME=`echo $PIDFILE | cut -c20-`
        DBNAME=${DBNAME%%.pid}
        echo -n " $DBNAME"
        stop_db
        start_db
      else
        echo -n " (failure: No such tunnel is running: $1)"
      fi
    done
  fi
  echo "."
  ;;
  restart)
    SCRIPT=$0
    shift
    $SCRIPT stop $*
    sleep 1
    $SCRIPT start $*
  ;;
  *)
  N=/etc/init.d/$NAME
  echo "Usage: $N {start|stop|restart|reload}" >&2
  exit 1
  ;;
esac

exit 0
