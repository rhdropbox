/*
 *  rhdropbox
 *
 *  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhdropbox.
 *
 *  rhdropbox is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhdropbox is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhdropbox. If not, see <http://www.gnu.org/licenses/>.
 */

#include "datatypes.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/inotify.h>

#include "log.h"
#include "sig_handler.h"
#include "options.h"

#include "client_list.h"

#include "daemon.h"
#include "utils.h"
#include "sysexec.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

enum cmd_id_enum { ADD, REMOVE, STATUS, LOG, LISTEN };
typedef enum cmd_id_enum cmd_id_t;

int send_response(int fd, const char* response)
{
  if(!response)
    return -1;

  if(fd < 0)
    return 0;

  int ret = send_string(fd, response);
  do {
    ret = write(fd, "\n", 1);
  } while(!ret || (ret == -1 && errno == EINTR));

  if(ret > 0)
    return 0;

  return ret;
}

int process_watch(int inotify_fd, read_buffer_t* buffer, watch_list_t* watch_lst, options_t* opt, client_t* client_lst, child_list_t* child_lst)
{
  log_printf(DEBUG, "something changed on watch descriptor '%d'", inotify_fd);

  int ret = 0;
  for(;;) {
    ret = read(inotify_fd, &buffer->buf[buffer->offset], sizeof(buffer->buf) - buffer->offset);
    if(!ret) {
      log_printf(ERROR, "read from inotfiy fd returned end of file!?");
      return -1;
    }
    if(ret == -1 && errno == EAGAIN)
      return 0;
    else if(ret < 0)
      return ret;

    buffer->offset += ret;
    for(;;) {
      if(buffer->offset >= sizeof(struct inotify_event)) {
        struct inotify_event* event;
        event = (struct inotify_event*)buffer->buf;
        u_int32_t len = sizeof(struct inotify_event) + event->len;
        if(buffer->offset >= len) {
          char* path = watch_list_find_path(watch_lst, event->wd);
          log_printf(DEBUG, "received event for %d (mask=0x%08X, cookie=0x%08X name[%u]='%s')", 
                     event->wd, event->mask, event->cookie, event->len, event->len > 0 ? event->name : "");

          if(event->mask & IN_DELETE_SELF || event->mask & IN_MOVE_SELF) {
            log_printf(NOTICE, "removing deleted or moved directory ('%s') from watch list", path);
            watch_list_rm(watch_lst, path);
          }
          else if(event->mask & IN_IGNORED) {
            log_printf(DEBUG, "ignoring inotify_rm_watch based events");
          }
          else {
            struct stat stat_buf;
            char buf[1024];
            snprintf(buf, 1024, "%s/%s", path, event->len > 0 ? event->name : "");
            int err = stat(buf, &stat_buf);
            if(err < 0) {
              log_printf(WARNING, "error at stat() for file %s", buf);
              buf[0] = 0;
            }
            else
              snprintf(buf, 1024, "%lu", stat_buf.st_size);

            char* const argv[] = { opt->script_, path, event->len > 0 ? event->name : "", buf, NULL };
            char* const evp[] = { NULL };
            rh_exec(opt->script_, argv, evp, child_lst, opt);

            snprintf(buf, 100, "new file in '%s', name='%s'", path, event->len > 0 ? event->name : "");
            log_printf(NOTICE, "%s, executing script %s", buf, opt->script_);
            client_t* client;
            int listener_cnt = 0;
            for(client = client_lst; client; client = client->next)
              if(client->status_listener) {
                send_response(client->fd, buf);
                listener_cnt++;
              }
            log_printf(DEBUG, "sent status to %d additional listeners", listener_cnt);
          }

          if(buffer->offset > len) {
            memmove(buffer->buf, &buffer->buf[len], buffer->offset - len);
            buffer->offset -= len;
          }
          else
            buffer->offset = 0;
        }
      }
      else
        break;
    }
  }
  return 0;
}

void send_status_watch_list(int fd, watch_list_t* watch_lst, client_t* client_lst)
{
  if(fd <= 0 && !client_lst)
    return;

  if(!watch_lst || !watch_lst->first_) {
    if(fd > 0)
      send_response(fd, "currently no paths in watch list");
    return;
  }

  char buf[100];
  watch_list_element_t* tmp = watch_lst->first_;
  while(tmp) {
    snprintf(buf, 100, "%3d: %s", tmp->watch_fd_, tmp->path_);
    if(fd > 0)
      send_response(fd, buf);
    client_t* client;
    for(client = client_lst; client; client = client->next)
      if(client->status_listener && client->fd != fd)
        send_response(client->fd, buf);
    tmp = tmp->next_;
  }
  if(fd > 0)
    send_response(fd, "end of watch list");
  int listener_cnt = 0;
  client_t* client;
  for(client = client_lst; client; client = client->next)
    if(client->status_listener && client->fd != fd) {
      send_response(client->fd, "end of watch list");
      listener_cnt++;
    }
  log_printf(DEBUG, "sent status to %d additional listeners", listener_cnt);

  return;
}

void process_cmd_add_remove(cmd_id_t cmd_id, const char* path, int fd, int inotify_fd, watch_list_t* watch_lst, client_t* client_lst)
{
  int wd = 0;
  int tmp_fd = watch_list_find_fd(watch_lst, path);
  if(cmd_id == ADD) {
    if(tmp_fd < 0)
      wd = inotify_add_watch(inotify_fd, path, IN_CLOSE_WRITE | IN_DELETE_SELF | IN_MOVE_SELF | IN_ONLYDIR);
    else {
      log_printf(DEBUG, "cmd_add_remove: path already in watch list");
      return;
    } 
  }
  else if(cmd_id == REMOVE) {
    if(tmp_fd >= 0)
      wd = inotify_rm_watch(inotify_fd, tmp_fd);
    else {
      log_printf(ERROR, "cmd_add_remove path not found in watch list");
      if(fd > 0)
        send_response(fd, "Error: path not found in watch list");
      return;
    }
  }
  else {
    log_printf(WARNING, "cmd_add_remove ignoring wrong cmd_id");
    return;
  }
  if(wd < 0) {
    log_printf(ERROR, "inotify %s '%s' failed: %s", cmd_id == ADD ? "adding" : "removing", path, strerror(errno));
    if(fd > 0)
      send_response(fd, "Error: add/remove failed");
    return;
  }
  
  int ret = 0;
  if(cmd_id == ADD) {
    ret = watch_list_add(watch_lst, wd, path);
    if(ret) {
      log_printf(ERROR, "can't add path to watch list");
      if(fd > 0)
        send_response(fd, "Error: can't add path to watch list");
    }
  }
  else {
    watch_list_rm(watch_lst, path);
  }
  
  if(!ret) {
    send_status_watch_list(-1, watch_lst, client_lst);
    log_printf(NOTICE, "inotify %s '%s'", cmd_id == ADD ? "added" : "removed", path);
  }
}

void process_cmd_status(int fd, watch_list_t* watch_lst, client_t* client_lst)
{
  send_status_watch_list(fd, watch_lst, client_lst);
}

void process_cmd_listen(const char* param, int fd, client_t* client_lst)
{
  client_t* listener = client_find(client_lst, fd);
  if(listener) {
    if(!param) {
      listener->status_listener = 1;
      listener->request_listener = 1;
    }
    else {
      if(!strncmp(param, "status", 6))
        listener->status_listener = 1;
      else if(!strncmp(param, "request", 7))
        listener->request_listener = 1;
      else if(!strncmp(param, "none", 4)) {
        listener->request_listener = 0;
        listener->status_listener = 0;
      }
      else {
        log_printf(DEBUG, "unkown listener type '%s'", param);
        send_response(fd, "Error: listen: unkown type");
        return;
      }
    }
    log_printf(DEBUG, "listener %d requests %s messages", fd, param ? param:"all");
  }
  else {

    log_printf(ERROR, "unable to add listener %d", fd);
    send_response(fd, "Error: listen: client not found in client list?!");
  }
}

int process_cmd(const char* cmd, int fd, int inotify_fd, watch_list_t* watch_lst, client_t* client_lst, options_t* opt)
{
  log_printf(DEBUG, "processing command from %d", fd);

  if(!cmd)
    return -1;
  
  cmd_id_t cmd_id;
  if(!strncmp(cmd, "add", 3))
    cmd_id = ADD;
  else if(!strncmp(cmd, "remove", 6))
    cmd_id = REMOVE;
  else if(!strncmp(cmd, "status", 6))
    cmd_id = STATUS;
  else if(!strncmp(cmd, "log", 3))
    cmd_id = LOG;
  else if(!strncmp(cmd, "listen", 6)) {
    cmd_id = LISTEN;
  }
  else {
    log_printf(WARNING, "unknown command '%s'", cmd);
    send_response(fd, "Error: unknown command");
    return 0;
  }
  char* param = strchr(cmd, ' ');
  if(param) 
    param++;

  if(cmd_id == ADD || cmd_id == REMOVE) {
    char* resp;
    asprintf(&resp, "Request: %s", cmd);
    if(resp) {
      char* linefeed = strchr(resp, '\n');
      if(linefeed) linefeed[0] = 0;
      client_t* client;
      int listener_cnt = 0;
      for(client = client_lst; client; client = client->next)
        if(client->request_listener && client->fd != fd) {
          send_response(client->fd, resp);
          listener_cnt++;
        }
      free(resp);
      log_printf(DEBUG, "sent request to %d additional listeners", listener_cnt);
    }
// else silently ignore memory alloc error
  }

  switch(cmd_id) {
  case ADD: 
  case REMOVE: process_cmd_add_remove(cmd_id, param, fd, inotify_fd, watch_lst, client_lst); break;
  case STATUS: process_cmd_status(fd, watch_lst, client_lst); break;
  case LOG: {
    if(param && param[0])
      log_printf(NOTICE, "ext msg: %s", param); 
    else
      log_printf(DEBUG, "ignoring empty ext log message");
    break;
  }
  case LISTEN: process_cmd_listen(param, fd, client_lst); break;
  }
  
  return 0;
}

int main_loop(int cmd_listen_fd, int inotify_fd, options_t* opt)
{
  log_printf(NOTICE, "entering main loop");

  fd_set readfds, tmpfds;
  FD_ZERO(&readfds);
  FD_SET(cmd_listen_fd, &readfds);
  FD_SET(inotify_fd, &readfds);
  int max_fd = (cmd_listen_fd < inotify_fd) ? inotify_fd : cmd_listen_fd;
  client_t* client_lst = NULL;

  read_buffer_t event_buffer;
  event_buffer.offset = 0;

  watch_list_t watch_lst;
  watch_list_init(&watch_lst);

  child_list_t child_lst;
  child_list_init(&child_lst);

  int sig_fd = signal_init();
  if(sig_fd < 0)
    return -1;
  FD_SET(sig_fd, &readfds);
  max_fd = (max_fd < sig_fd) ? sig_fd : max_fd;

  string_list_element_t* dir = opt->dirs_.first_;
  for(;dir;dir=dir->next_)
    process_cmd_add_remove(ADD, dir->string_, -1, inotify_fd, &watch_lst, client_lst);

  struct timeval timeout;
  int return_value = 0;
  while(!return_value) {
    memcpy(&tmpfds, &readfds, sizeof(tmpfds));

    timeout.tv_sec = 0;
    timeout.tv_usec = 200000;
    int ret = select(max_fd+1, &tmpfds, NULL, NULL, &timeout);
    if(ret == -1 && errno != EINTR) {
      log_printf(ERROR, "select returned with error: %s", strerror(errno));
      return_value = -1;
      break;
    }
    if(ret == -1)
      continue;
    if(!ret) {
      rh_waitpid(&child_lst, opt);
      continue;
    }

    if(FD_ISSET(sig_fd, &tmpfds)) {
      if(signal_handle()) {
        return_value = 1;
        break;
      }
    }
   
    if(FD_ISSET(inotify_fd, &tmpfds)) {
      return_value = process_watch(inotify_fd, &event_buffer, &watch_lst, opt, client_lst, &child_lst);
      if(return_value)
        break;
    }

    if(FD_ISSET(cmd_listen_fd, &tmpfds)) {
      int new_fd = accept(cmd_listen_fd, NULL, NULL);
      if(new_fd < 0) {
        log_printf(ERROR, "accept returned with error: %s", strerror(errno));
        return_value = -1;
        break;
      }  
      log_printf(DEBUG, "new command connection (fd=%d)", new_fd);
      FD_SET(new_fd, &readfds);
      max_fd = (max_fd < new_fd) ? new_fd : max_fd;
      fcntl(new_fd, F_SETFL, O_NONBLOCK);
      int o_sndbuf = 128*1024;
      setsockopt(new_fd, SOL_SOCKET, SO_SNDBUF, &o_sndbuf, sizeof o_sndbuf);
      client_add(&client_lst, new_fd);
    }

    client_t* lst = client_lst;
    while(lst) {
      if(FD_ISSET(lst->fd, &tmpfds)) {
        return_value = nonblock_recvline(&(lst->buffer), lst->fd, inotify_fd, &watch_lst, client_lst, opt);
        if(return_value == 2) {
          log_printf(DEBUG, "removing closed command connection (fd=%d)", lst->fd);
          client_t* deletee = lst;
          lst = lst->next;
          FD_CLR(deletee->fd, &readfds);
          client_remove(&client_lst, deletee->fd);
          return_value = 0;
          continue;
        }
        if(return_value)
          break;

      }
      if(lst)
        lst = lst->next;
    }
  }

  child_list_clear(&child_lst);
  watch_list_clear(&watch_lst);
  client_clear(&client_lst);
  signal_stop();
  return return_value;
}

int main(int argc, char* argv[])
{
  log_init();

  options_t opt;
  int ret = options_parse(&opt, argc, argv);
  if(ret) {
    if(ret > 0) {
      fprintf(stderr, "syntax error near: %s\n\n", argv[ret]);
    }
    if(ret == -2) {
      fprintf(stderr, "memory error on options_parse, exiting\n");
    }

    if(ret != -2)
      options_print_usage();

    options_clear(&opt);
    log_close();
    exit(ret);
  }
  string_list_element_t* tmp = opt.log_targets_.first_;
  if(!tmp) {
    log_add_target("syslog:3,rhdropbox,daemon");
  }
  else {
    while(tmp) {
      ret = log_add_target(tmp->string_);
      if(ret) {
        switch(ret) {
        case -2: fprintf(stderr, "memory error on log_add_target, exitting\n"); break;
        case -3: fprintf(stderr, "unknown log target: '%s', exitting\n", tmp->string_); break;
        case -4: fprintf(stderr, "this log target is only allowed once: '%s', exitting\n", tmp->string_); break;
        default: fprintf(stderr, "syntax error near: '%s', exitting\n", tmp->string_); break;
        }
        
        options_clear(&opt);
        log_close();
        exit(ret);
      }
      tmp = tmp->next_;
    }
  }
  log_printf(NOTICE, "just started...");
  if(options_parse_post(&opt)) {
    options_clear(&opt);
    log_close();
    exit(-1);
  }

  priv_info_t priv;
  if(opt.username_)
    if(priv_init(&priv, opt.username_, opt.groupname_)) {
      options_clear(&opt);
      log_close();
      exit(-1);
    }

  FILE* pid_file = NULL;
  if(opt.pid_file_) {
    pid_file = fopen(opt.pid_file_, "w");
    if(!pid_file) {
      log_printf(WARNING, "unable to open pid file: %s", strerror(errno));
    }
  }

  if(opt.chroot_dir_)
    if(do_chroot(opt.chroot_dir_)) {
      options_clear(&opt);
      log_close();
      exit(-1);
    }
  if(opt.username_)
    if(priv_drop(&priv)) {
      options_clear(&opt);
      log_close();
      exit(-1);
    }

  if(opt.daemonize_) {
    pid_t oldpid = getpid();
    daemonize();
    log_printf(INFO, "running in background now (old pid: %d)", oldpid);
  }

  if(pid_file) {
    pid_t pid = getpid();
    fprintf(pid_file, "%d", pid);
    fclose(pid_file);
  }

  int cmd_listen_fd = init_command_socket(opt.command_sock_);
  if(cmd_listen_fd < 0) {
    options_clear(&opt);
    log_close();
    exit(-1);
  }

  int inotify_fd = create_inotify();
  if(inotify_fd < 0) {
    close(cmd_listen_fd);
    options_clear(&opt);
    log_close();
    exit(-1);
  }  

  ret = main_loop(cmd_listen_fd, inotify_fd, &opt);

  close(cmd_listen_fd);
  close(inotify_fd);

  if(!ret)
    log_printf(NOTICE, "normal shutdown");
  else if(ret < 0)
    log_printf(NOTICE, "shutdown after error");
  else
    log_printf(NOTICE, "shutdown after signal");

  options_clear(&opt);
  log_close();

  return ret;
}
