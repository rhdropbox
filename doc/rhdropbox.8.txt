rhdropbox(8)
============

NAME
----

rhdropbox - radio helsinki dropbox daemon


SYNOPSIS
--------

....
rhdropbox
  [ -h|--help ]
  [ -D|--nodaemonize ]
  [ -u|--username <username> ]
  [ -g|--groupname <groupname> ]
  [ -C|--chroot <path> ]
  [ -P|--write-pid <filename> ]
  [ -L|--log <target>:<level>[,<param1>[,<param2>[..]]] ]
  [ -s|--command-sock <unix sock> ]
  [ -x|--script <script> ]
  [ -m|--max-children <#of children> ]
  [ -d|--dir <path> ]
....


DESCRIPTION
-----------

*rhdropbox* is a small daemon which can be used to watch one or more 
directories for new files. When a new file is detected a external program. 
Additional directories can be added to the damons watch list using a simple
unix socket based command interface.


OPTIONS
-------

The following options can be passed to the *rhdropbox* daemon:

*-D, --nodaemonize*::
   This option instructs *rhdropbox* to run in foreground
   instead of becoming a daemon which is the default.

*-u, --username '<username>'*::
   run as this user. If no group is specified (*-g*) the default group of 
   the user is used. The default is to not drop privileges.

*-g, --groupname '<groupname>'*::
   run as this group. If no username is specified (*-u*) this gets ignored.
   The default is to not drop privileges.

*-C, --chroot '<path>'*::
   Instruct *rhdropbox* to run in a chroot jail. The default is 
   to not run in chroot.

*-P, --write-pid '<filename>'*::
   Instruct *rhdropbox* to write it's pid to this file. The default is 
   to not create a pid file.

*-L, --log '<target>:<level>[,<param1>[,<param2>[..]]]'*::
   add log target to logging system. This can be invoked several times
   in order to log to different targets at the same time. Every target 
   hast its own log level which is a number between 0 and 5. Where 0 means
   disabling log and 5 means debug messages are enabled. +
   The file target can be used more the once with different levels.
   If no target is provided at the command line a single target with the 
   config *syslog:3,rhdropbox,daemon* is added. +
   The following targets are supported:

   'syslog';; log to syslog daemon, parameters <level>[,<logname>[,<facility>]]
   'file';; log to file, parameters <level>[,<path>]
   'stdout';; log to standard output, parameters <level>
   'stderr';; log to standard error, parameters <level> 

*-s|--command-sock '<unix sock>'*::
   *rhdropbox* will listen on this unix socket for incoming commands. See next
   chapter for details. The default is /var/run/rhdropbox/cmd.sock

*-x|--script '<script>'*::
   The script which should be called when a new file is found. The following parameter
   are passed to the scipt:

   '<path>';; the path to the folder containing the new file
   '<filename>';; the name of the new file relative to <path>
   '<filesize>';; the size of the file right after the event was received, this can be
                  used to determine wheter the file is complete. The script should
                  consider a file to be complete when it size doesn't change 3-5seconds
                  after the event got received.

*-m|--max-children '<#of children>'*::
   Limits the total concurrent executions of *-x|--script*. A value of 0 means no limit.

*-d|--dir '<path>'*::
   Add '<path>' to the watch list. This can be invoked serveral times.


COMMAND INTERFACE
-----------------

*rhdropbox* is listening for incoming commands at the unix socket speciefied by the 
*-s|--command-sock* parameter. You can connect to this socket for example using
socat:

 # socat UNIX-CONNECT:/var/run/rhdropbox/rhdropbox.sock STDIO

All commands have to end with '\n'. The following commands are supported:

*add <path>*::
   Add a path to the list of directories to be watched. If the command succeds nothing
   is returned. In case of an error a string of the form "Error: <reason>\n" is returned.

*remove <path>*::
   Remove the path from the list of directories. If the command succeds nothing
   is returned. In case of an error a string of the form "Error: <reason>\n" is returned.

*status*::
   Returns the complete list of all directories of the watch list.

*listen [request|status|none]*::
   Add the client to the status and or request listeners. When no parameter is passed to
   this command the client listens to all messages. 
   A status listener gets a list of all directories whenever somebody else adds or removes
   a path to the watch list. It also receives the list when somebody sends a status command
   to the daemon. 
   A request listener also receives add and remove requests by other clients connected to
   the command interface.

*log <text>*::
   Print <text> to the logtargets of the daemon.


BUGS
----
Most likely there are some bugs in *rhdropbox*. If you find a bug, please let
the developers know at equinox@helsinki.at. Of course, patches are preferred.


AUTHORS
-------

Christian Pointner <equinox@heslinki.at>


COPYING
-------

Copyright \(C) 2009 Christian Pointner. This  program is  free 
software: you can redistribute it and/or modify it under the terms 
of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or any later version.
