/*
 *  rhdropbox
 *
 *  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhdropbox.
 *
 *  rhdropbox is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhdropbox is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhdropbox. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RHDROPBOX_utils_h_INCLUDED
#define RHDROPBOX_utils_h_INCLUDED

#include "client_list.h"
#include "watch_list.h"
#include "options.h"
#include <termios.h>

int init_command_socket(const char* path);
int connect_command_socket(const char* path);
int send_string(int fd, const char* string);
int nonblock_recvline(read_buffer_t* buffer, int fd, int inotify_fd, watch_list_t* watch_lst, client_t* client_lst, options_t* opt);
int create_inotify();

#endif
