/*
 *  rhdropbox
 *
 *  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhdropbox.
 *
 *  rhdropbox is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhdropbox is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhdropbox. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RHDROPBOX_watch_list_h_INCLUDED
#define RHDROPBOX_watch_list_h_INCLUDED

struct watch_list_element_struct {
  char* path_;
  int watch_fd_;
  struct watch_list_element_struct* next_;
};
typedef struct watch_list_element_struct watch_list_element_t;

struct watch_list_struct {
  watch_list_element_t* first_;
};
typedef struct watch_list_struct watch_list_t;

void watch_list_init(watch_list_t* list);
void watch_list_clear(watch_list_t* list);
int watch_list_add(watch_list_t* list, int watch_fd, const char* path);
void watch_list_rm(watch_list_t* list, const char* path);
char* watch_list_find_path(watch_list_t* list, int watch_fd);
int watch_list_find_fd(watch_list_t* list, const char* path);

void watch_list_print(watch_list_t* list, const char* head, const char* sep, const char* tail);

#endif
