/*
 *  rhdropbox
 *
 *  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhdropbox.
 *
 *  rhdropbox is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhdropbox is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhdropbox. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RHDROPBOX_sysexec_h_INCLUDED
#define RHDROPBOX_sysexec_h_INCLUDED

#include <sys/types.h>
#include "options.h"

struct child_list_element_struct {
  pid_t pid_;
  char* script_;
  int err_fd_;
  int running_;
  char** argv_;
  char** evp_;
  struct child_list_element_struct* next_;
};
typedef struct child_list_element_struct child_list_element_t;

struct child_list_struct {
  child_list_element_t* first_;
};
typedef struct child_list_struct child_list_t;

void child_list_init(child_list_t* list);
void child_list_clear(child_list_t* list);
child_list_element_t* child_list_new(const char* script, char* const argv[], char* const evp[]);
child_list_element_t* child_list_add(child_list_t* list, const char* script, char* const argv[], char* const evp[]);
void child_list_rm(child_list_t* list, child_list_element_t* child);
void child_list_rm_pid(child_list_t* list, pid_t pid);
child_list_element_t* child_list_find(child_list_t* list, pid_t pid);
int child_list_num_running(child_list_t* list);

int rh_exec(const char* script, char* const argv[], char* const evp[], child_list_t* child_lst, options_t* opt);
int rh_exec_child(child_list_element_t* child);
int rh_waitpid(child_list_t* child_lst, options_t* opt);

#endif
