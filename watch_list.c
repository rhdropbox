/*
 *  rhdropbox
 *
 *  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhdropbox.
 *
 *  rhdropbox is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhdropbox is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with rhdropbox. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "watch_list.h"

void watch_list_init(watch_list_t* list)
{
  if(!list)
    return;
  
  list->first_ = NULL;
}

void watch_list_clear(watch_list_t* list)
{
  if(!list)
    return;

  while(list->first_) {
    watch_list_element_t* tmp;
    tmp = list->first_;
    list->first_ = tmp->next_;
    if(tmp->path_)
      free(tmp->path_);
    free(tmp);
  }
}

int watch_list_add(watch_list_t* list, int watch_fd, const char* string)
{
  if(!list)
    return -1;

  if(!list->first_) {
    list->first_ = malloc(sizeof(watch_list_element_t));
    if(!list->first_)
      return -2;

    list->first_->next_ = 0;
    list->first_->watch_fd_ = watch_fd;
    list->first_->path_ = strdup(string);
    if(!list->first_->path_) {
      free(list->first_);
      list->first_ = NULL;
      return -2;
    }
  }
  else {
    watch_list_element_t* tmp = list->first_;
    while(tmp->next_)
      tmp = tmp->next_;

    tmp->next_  = malloc(sizeof(watch_list_element_t));
    if(!tmp->next_)
      return -2;

    tmp->next_->next_ = 0;
    tmp->next_->watch_fd_ = watch_fd;
    tmp->next_->path_ = strdup(string);
    if(!tmp->next_->path_) {
      free(tmp->next_);
      tmp->next_ = NULL;
      return -2;
    }
  }
  return 0;
}

void watch_list_rm(watch_list_t* list, const char* path)
{
  if(!list || !path)
    return;
  
  watch_list_element_t* tmp = NULL;
  if(list->first_->path_ && !strcmp(path, list->first_->path_)) {
    tmp = list->first_;
    list->first_ = list->first_->next_;
    
    if(tmp->path_)
      free(tmp->path_);
    free(tmp);
    return;
  }

  watch_list_element_t* prev = list->first_;
  tmp = list->first_->next_;
  while(tmp) {
    if(tmp->path_ && !strcmp(path, tmp->path_)) {
      prev->next_ = tmp->next_;

      if(tmp->path_)
        free(tmp->path_);
      free(tmp);
      return;      
    }
    prev = tmp;
    tmp = tmp->next_;
  }
}

char* watch_list_find_path(watch_list_t* list, int watch_fd)
{
  if(!list)
    return NULL;
  
  watch_list_element_t* tmp = list->first_;
  while(tmp) {
    if(tmp->watch_fd_ == watch_fd)
      return tmp->path_;
    tmp = tmp->next_;
  }
  
  return NULL;
}

int watch_list_find_fd(watch_list_t* list, const char* path)
{
  if(!list || !path)
    return -1;
  
  watch_list_element_t* tmp = list->first_;
  while(tmp) {
    if(tmp->path_ && !strcmp(path, tmp->path_))
      return tmp->watch_fd_;
    tmp = tmp->next_;
  }
  
  return -1;
}

void watch_list_print(watch_list_t* list, const char* head, const char* sep, const char* tail)
{
  if(!list)
    return;
  
  watch_list_element_t* tmp = list->first_;
  while(tmp) {
    printf("%s%d%s%s%s", head, tmp->watch_fd_, sep, tmp->path_, tail);
    tmp = tmp->next_;
  }
}
