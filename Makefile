##
##  rhdropbox
##
##  Copyright (C) 2009 Christian Pointner <equinox@helsinki.at>
##
##  This file is part of rhdropbox.
##
##  rhdropbox is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  any later version.
##
##  rhdropbox is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with rhdropbox. If not, see <http://www.gnu.org/licenses/>.
##

ifneq ($(MAKECMDGOALS),distclean)
include include.mk
endif

EXECUTABLE := rhdropbox

OBJ := log.o \
       sig_handler.o \
       string_list.o \
       utils.o \
       client_list.o \
       watch_list.o \
       options.o \
       sysexec.o \
       rhdropbox.o

SRC := $(OBJ:%.o=%.c) 

.PHONY: clean distclean

all: $(EXECUTABLE)

%.d: %.c
	@set -e; rm -f $@;                                 	 \
  $(CC) -MM $(CFLAGS) $< > $@.$$$$;                  	 \
  sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@;	 \
  rm -f $@.$$$$; echo '(re)building $@'

ifneq ($(MAKECMDGOALS),distclean)
-include $(SRC:%.c=%.d)
endif

$(EXECUTABLE): $(OBJ)
	$(CC) $(OBJ) -o $@ $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $<


distclean: cleanall
	find . -name *.o -exec rm -f {} \;
	find . -name "*.\~*" -exec rm -rf {} \;
	rm -f include.mk

clean:
	rm -f *.o
	rm -f *.d
	rm -f *.d.*
	rm -f $(EXECUTABLE)

cleanall: clean
	$(MAKE) --directory="doc/" clean

manpage:
	$(MAKE) --directory="doc/" manpage


INSTALL_TARGETS := install-bin install-etc
REMOVE_TARGETS := remove-bin remove-etc

ifdef MANDIR
INSTALL_TARGETS += install-man
REMOVE_TARGETS += remove-man
endif

ifdef EXAMPLESDIR
INSTALL_TARGETS += install-examples
REMOVE_TARGETS += remove-examples
endif

install: all $(INSTALL_TARGETS)

install-bin: $(EXECUTABLE)
	$(INSTALL) -d $(DESTDIR)$(BINDIR)
	$(INSTALL) -m 755 $(EXECUTABLE) $(DESTDIR)$(BINDIR)

install-etc:
	$(INSTALL) -d $(DESTDIR)$(ETCDIR)/$(EXECUTABLE)
	@ echo "example configurations can be found at $(EXAMPLESDIR)/$(EXECUTABLE)" > $(DESTDIR)$(ETCDIR)/$(EXECUTABLE)/README

install-man: manpage
	$(INSTALL) -d $(DESTDIR)$(MANDIR)/man8/
	$(INSTALL) -m 644 doc/rhdropbox.8 $(DESTDIR)$(MANDIR)/man8/$(EXECUTABLE).8

install-examples:
	$(INSTALL) -d $(DESTDIR)$(EXAMPLESDIR)/$(EXECUTABLE)
	$(INSTALL) -m 644 etc/rhdropbox/autostart $(DESTDIR)$(EXAMPLESDIR)/$(EXECUTABLE)/autostart
	@( cd 'etc/rhdropbox/' ;                                                                         \
     for dir in `ls`; do                                                                           \
       if [ -d $$dir ]; then                                                                       \
         echo "install $$dir configuration" ;                                                      \
         cd $$dir ;                                                                                \
         $(INSTALL) -d $(DESTDIR)$(EXAMPLESDIR)/$(EXECUTABLE)/$$dir ;                              \
         $(INSTALL) -m 644 config $(DESTDIR)$(EXAMPLESDIR)/$(EXECUTABLE)/$$dir/config ;            \
         if [ -e 'newfile.sh' ]; then                                                              \
           $(INSTALL) -m 755 newfile.sh $(DESTDIR)$(EXAMPLESDIR)/$(EXECUTABLE)/$$dir/newfile.sh ;  \
         fi ;                                                                                      \
         cd .. ;                                                                                   \
       fi ;                                                                                        \
     done                                                                                          \
   )

uninstall: remove

remove: $(REMOVE_TARGETS)

remove-bin:
	rm -f $(DESTDIR)$(BINDIR)/$(EXECUTABLE)

remove-etc:

remove-examples:
	rm -rf $(DESTDIR)$(EXAMPLESDIR)/$(EXECUTABLE)/

remove-man:
	rm -f $(DESTDIR)$(MANDIR)/man8/$(EXECUTABLE).8

purge: remove
	rm -rf $(DESTDIR)$(ETCDIR)/$(EXECUTABLE)/
