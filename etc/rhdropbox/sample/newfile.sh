#!/bin/sh

DIR=$1
FILENAME=$2
SIZE=$3

if [ -z "$FILENAME" ]; then
  exit 1;
fi

echo "new file of size $SIZE detected: $DIR/$FILENAME" >> /tmp/rhdropbox-sample.log
sleep 5
rm -f $DIR/$FILENAME >> /tmp/rhdropbox-sample.log 2>&1

exit 0

